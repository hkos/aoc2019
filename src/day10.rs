use std::f64::consts::PI;

pub fn part1() {
    println!("Day 10, Part 1");

    let (map, _) = Map::load(&map());
    println!("asteroids in view: {:?}\n", map.how_many_in_view());
}

pub fn part2() {
    println!("Day 10, Part 2");

    let (map, _) = Map::load(&map());
    let b = Coord(8, 16);
    let mut base = map.base(&b);

    let foo = base.shoot(200);
    println!("200th {:?}", foo);
}


#[derive(PartialEq, Clone, Debug)]
struct Coord(usize, usize);

impl Coord {
    fn dist(&self, other: &Self) -> f64 {
        ((self.0 as f64 - other.0 as f64).powi(2)
            + (self.1 as f64 - other.1 as f64).powi(2)).sqrt()
    }
}

struct Map { asteroids: Vec<Coord> }

#[derive(Clone)]
struct Base<'a> {
    base: Coord,
    angles: Vec<(f64, &'a Coord)>,
}

impl<'a> Base<'a> {
    fn diff_angles(&self) -> usize {
        let mut angles = Vec::new();

        self.angles.iter().for_each(|(a, _)| {
            if !angles.contains(a) {
                angles.push(*a);
            }
        });

        angles.len()
    }

    fn next_angle(&self, old: f64) -> Option<f64> {
        if self.angles.is_empty() {
            return None;
        }

        let mut found: Option<f64> = None;
        self.angles.iter().for_each(|(a, _)| {
            if *a > old {
                if found == None {
                    found = Some(*a);
                } else {
                    if found.unwrap() > *a {
                        found = Some(*a);
                    }
                }
            }
        });

        if found.is_some() {
            found
        } else {
            self.next_angle(-4.0) // FIXME
        }
    }


    fn index_closest(&mut self, angle: f64) -> Option<usize> {
        let dists: Vec<_> = self.angles.iter()
            .filter(|(a, _)| a == &angle)
            .map(|(_, c)| (c.dist(&self.base), c))
            .collect();

        let mut min = None; // FIXME
        dists.iter().map(|(dist, _)| dist)
            .for_each(|&c| if min == None || Some(c) < min { min = Some(c) });

        if min.is_none() {
            return None;
        }

        let res = dists.iter().find(|(dist, _)| Some(*dist) == min).unwrap();

        self.angles.iter().enumerate()
            .find(|(_, (a, c))| a == &angle && c == res.1)
            .map(|(i, _)| i)
    }

    fn shoot(&mut self, count: usize) -> Option<Coord> {
        let mut cur = Some(PI / 2.0);
        let mut n = 0;

        loop {
            // shoot
            let i = self.index_closest(cur.unwrap());
            if let Some(i) = i {
                n += 1;
                if n == count {
                    return Some(self.angles[i].1.clone());
                }
                self.angles.remove(i);
            }

            // find next angle
            cur = self.next_angle(cur.unwrap());

            if cur.is_none() {
                return None;
            }
        }
    }
}

impl Map {
    fn how_many_in_view(&self) -> usize {
        let mut max = 0;

        for b in self.asteroids.iter() {
            let b2 = self.base(b);
            let diff = b2.diff_angles();
            if diff > max {
                max = diff;
                println!("new max {} for {:?}", max, b);
            }
        }
        max
    }

    pub fn base(&self, base: &Coord) -> Base {
        let mut angles = Vec::new();

        self.asteroids.iter().filter(|&c| c != base).for_each(|c| {
            let x = base.0 as f64 - c.0 as f64;
            let y = base.1 as f64 - c.1 as f64;

            let a = y.atan2(x);

            angles.push((a, c));
        });

        Base { base: base.clone(), angles }
    }

    fn load(s: &str) -> (Self, Option<Coord>) {
        let mut asteroids = Vec::new();
        let mut base = None;

        s.lines().enumerate()
            .for_each(|(y, line)|
                line.chars().enumerate().for_each(|(x, c)| {
                    match c {
                        '#' => asteroids.push(Coord(x, y)),
                        'X' => base = Some(Coord(x, y)),
                        '.' => (),
                        _ => panic!("unepexcted char {}", c)
                    }
                }));


        (Map { asteroids }, base)
    }
}


#[cfg(test)]
mod tests {
    use crate::day10::{Map, map, Coord};

    #[test]
    fn test_small1() {
        let test = ".#..#
.....
#####
....#
...##";
        let (map, _) = Map::load(test);
        assert_eq!(map.how_many_in_view(), 8);
    }


    #[test]
    fn test_small2() {
        let test = "......#.#.
#..#.#....
..#######.
.#.#.###..
.#..#.....
..#....#.#
#..#....#.
.##.#..###
##...#..#.
.#....####";
        let (map, _) = Map::load(test);
        assert_eq!(map.how_many_in_view(), 33);
    }

    #[test]
    fn test_small5() {
        let (map, _) = Map::load(&test1());
        assert_eq!(map.how_many_in_view(), 210);
    }

    fn test1() -> String {
        ".#..##.###...#######
##.############..##.
.#.######.########.#
.###.#######.####.#.
#####.##.#.##.###.##
..#####..#.#########
####################
#.####....###.#.#.##
##.#################
#####.##.###..####..
..######..##.#######
####.##.####...##..#
.#####..#.######.###
##...#.##########...
#.##########.#######
.####.#.###.###.#.##
....##.##.###..#####
.#.#.###########.###
#.#.#.#####.####.###
###.##.####.##.#..##".to_string()
    }

    #[test]
    fn test_p2_small1() {
        let test = ".#....#####...#..
##...##.#####..##
##...#...#.#####.
..#.....X...###..
..#.#.....#....##";
        let (map, base) = Map::load(test);
        if let Some(b) = base {
            let mut base = map.base(&b);
            let foo = base.shoot(1);
            assert_eq!(foo, Some(Coord(8, 1)));
        }
    }

    #[test]
    fn test_p2_small2() {
        let (map, base) = Map::load(&test1());
        if let Some(b) = base {
            let base = map.base(&b);

            assert_eq!(base.clone().shoot(1), Some(Coord(11, 12)));
            assert_eq!(base.clone().shoot(2), Some(Coord(12, 1)));
            assert_eq!(base.clone().shoot(3), Some(Coord(12, 2)));
            assert_eq!(base.clone().shoot(10), Some(Coord(12, 8)));
            assert_eq!(base.clone().shoot(20), Some(Coord(16, 0)));
            assert_eq!(base.clone().shoot(50), Some(Coord(16, 9)));
            assert_eq!(base.clone().shoot(100), Some(Coord(10, 16)));
            assert_eq!(base.clone().shoot(199), Some(Coord(9, 6)));
            assert_eq!(base.clone().shoot(200), Some(Coord(8, 2)));
            assert_eq!(base.clone().shoot(201), Some(Coord(10, 9)));
            assert_eq!(base.clone().shoot(201), Some(Coord(11, 1)));
            assert_eq!(base.clone().shoot(300), None);
        }
    }


    #[test]
    fn test_p1() {
        let (map, _) = Map::load(&map());
        assert_eq!(map.how_many_in_view(), 214);
    }
}

fn map() -> String {
    ".###.###.###.#####.#
#####.##.###..###..#
.#...####.###.######
######.###.####.####
#####..###..########
#.##.###########.#.#
##.###.######..#.#.#
.#.##.###.#.####.###
##..#.#.##.#########
###.#######.###..##.
###.###.##.##..####.
.##.####.##########.
#######.##.###.#####
#####.##..####.#####
##.#.#####.##.#.#..#
###########.#######.
#.##..#####.#####..#
#####..#####.###.###
####.#.############.
####.#.#.##########.
".to_string()
}