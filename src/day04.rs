use std::convert::TryInto;

pub fn part1() {
    println!("Day 04, Part 1");

    println!("How many: {}\n",
             count_valid_in(356261, 846303, valid_password1));
}

pub fn part2() {
    println!("Day 04, Part 2");
    println!("How many with exactly two adjacent: {}\n",
             count_valid_in(356261, 846303, valid_password2));
}

fn count_valid_in<F>(first: u32, last: u32, f: F) -> usize
    where F: Fn(u32) -> bool {
    (first..=last).filter(|&n| f(n)).count()
}

/// predicate for valid passwords from part 1
fn valid_password1(number: u32) -> bool {
    let number: [u8; 6] = split(number);
    increasing(&number) && has_double(&number)
}

/// predicate for valid passwords from part 2
fn valid_password2(number: u32) -> bool {
    let number: [u8; 6] = split(number);
    increasing(&number) && has_exactly_double(&number)
}

fn split(number: u32) -> [u8; 6] {
    let mut v: Vec<u8> = Vec::new();
    let mut n = number;

    while n > 0 {
        v.insert(0, (n % 10) as u8);
        n /= 10;
    }

    v[0..6].try_into().unwrap()
}

fn increasing(n: &[u8; 6]) -> bool {
    n.windows(2).all(|x| x[0] <= x[1])
}

fn has_double(n: &[u8; 6]) -> bool {
    n.windows(2).any(|x| x[0] == x[1])
}

/// this isn't particularly nice code ... but it's probably at least going
/// to compile to rather fast assembly :)
fn has_exactly_double(n: &[u8; 6]) -> bool {
    (n[0] == n[1] && n[2] != n[0])
        || (n[1] == n[2] && n[0] != n[1] && n[2] != n[3])
        || (n[2] == n[3] && n[1] != n[2] && n[3] != n[4])
        || (n[3] == n[4] && n[2] != n[3] && n[4] != n[5])
        || (n[4] == n[5] && n[3] != n[4])
}


#[cfg(test)]
mod tests {
    use crate::day04::{valid_password1, count_valid_in, valid_password2};

    #[test]
    fn test_examples1() {
        assert_eq!(valid_password1(111111), true);
        assert_eq!(valid_password1(223450), false);
        assert_eq!(valid_password1(123789), false);

        assert_eq!(valid_password1(122345), true);
        assert_eq!(valid_password1(111123), true);
        assert_eq!(valid_password1(135679), false);
    }

    #[test]
    fn test_examples2() {
        assert_eq!(valid_password2(112233), true);
        assert_eq!(valid_password2(123444), false);
        assert_eq!(valid_password2(111122), true);
    }

    #[test]
    fn test_part1() {
        assert_eq!(count_valid_in(356261, 846303, valid_password1), 544);
    }

    #[test]
    fn test_part2() {
        assert_ne!(count_valid_in(356261, 846303, valid_password2), 544);
        assert_eq!(count_valid_in(356261, 846303, valid_password2), 334);
    }
}
