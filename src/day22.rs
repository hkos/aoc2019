pub fn part1() {
    println!("Day 22, Part 1");

    let deck = Deck::new(10007).perform(&input());

    let pos = deck.cards.iter().enumerate()
        .find(|(_, &i)| i == 2019).unwrap().0;

    println!("position of card 2019 is {}\n", pos);
}

pub fn part2() {
    println!("Day 2, Part 2");
}

#[derive(Clone, PartialEq, Debug)]
struct Deck {
    cards: Vec<u16>,
}

impl Deck {
    fn new(size: usize) -> Self {
        let mut cards = Vec::new();

        for i in 0..size {
            cards.push(i as u16);
        }

        Deck { cards }
    }

    fn deal_into(&self) -> Self {
        Deck { cards: self.cards.iter().rev().map(|c| *c).collect() }
    }

    fn cut_n(&self, n: i16) -> Self {
        let mut cards = Vec::with_capacity(self.cards.len());

        let n = if n < 0 {
            (n + self.cards.len() as i16) as usize
        } else {
            n as usize
        };

        for i in n..self.cards.len() {
            cards.push(self.cards[i]);
        }
        for i in 0..n {
            cards.push(self.cards[i]);
        }

        Deck { cards }
    }

    fn deal_with_increment(&self, inc: usize) -> Self {
        let mut cards = Vec::with_capacity(self.cards.len());
        (0..self.cards.len()).for_each(|_| cards.push(0));

        let mut pos = 0;
        for i in 0..self.cards.len() {
            cards[pos] = self.cards[i];
            pos = (pos + inc) % self.cards.len();
        }

        Deck { cards }
    }

    fn do_step(&self, line: &str) -> Self {
        if line == "deal into new stack" {
            self.deal_into()
        } else if line.starts_with("deal with increment") {
            let l = line.split(" ").collect::<Vec<_>>();
            let num = l.last();
            let num = num.unwrap().parse::<usize>().unwrap();
            self.deal_with_increment(num)
        } else if line.starts_with("cut") {
            let l = line.split(" ").collect::<Vec<_>>();
            let num = l.last();
            let num = num.unwrap().parse::<i16>().unwrap();
            self.cut_n(num)
        } else {
            unreachable!();
        }
    }

    fn perform(&self, process: &str) -> Self {
        let mut new = self.clone();

        for line in process.lines() {
            new = new.do_step(line);
        }

        new
    }
}


#[cfg(test)]
mod tests {
    use crate::day22::{Deck, input};

    #[test]
    fn test_small1() {
        let deck = Deck::new(10);
        println!("deck {:?}", deck.cards);

        let deck2 = deck.deal_into();
        println!("deck2 {:?}", deck2.cards);
        assert_eq!(deck2.cards, vec![9, 8, 7, 6, 5, 4, 3, 2, 1, 0]);

        let deck3 = deck.cut_n(3);
        println!("deck3 {:?}", deck3.cards);
        assert_eq!(deck3.cards, vec![3, 4, 5, 6, 7, 8, 9, 0, 1, 2]);

        let deck4 = deck.cut_n(-4);
        println!("deck4 {:?}", deck4.cards);
        assert_eq!(deck4.cards, vec![6, 7, 8, 9, 0, 1, 2, 3, 4, 5]);

        let deck5 = deck.deal_with_increment(3);
        println!("deck5 {:?}", deck5.cards);
        assert_eq!(deck5.cards, vec![0, 7, 4, 1, 8, 5, 2, 9, 6, 3]);
    }


    #[test]
    fn test_small2() {
        let deck = Deck::new(10);
        let deck2 = deck.perform("deal with increment 7
deal into new stack
deal into new stack");
        println!("deck2 {:?}", deck2.cards);
        assert_eq!(deck2.cards, vec![0, 3, 6, 9, 2, 5, 8, 1, 4, 7]);
    }


    #[test]
    fn test_small3() {
        let deck = Deck::new(10);
        let deck2 = deck.perform("cut 6
deal with increment 7
deal into new stack");
        println!("deck2 {:?}", deck2.cards);
        assert_eq!(deck2.cards, vec![3, 0, 7, 4, 1, 8, 5, 2, 9, 6]);
    }

    #[test]
    fn test_small4() {
        let deck = Deck::new(10);
        let deck2 = deck.perform("deal with increment 7
deal with increment 9
cut -2");
        println!("deck2 {:?}", deck2.cards);
        assert_eq!(deck2.cards, vec![6, 3, 0, 7, 4, 1, 8, 5, 2, 9]);
    }


    #[test]
    fn test_small5() {
        let deck = Deck::new(10);
        let deck2 = deck.perform("deal into new stack
cut -2
deal with increment 7
cut 8
cut -4
deal with increment 7
cut 3
deal with increment 9
deal with increment 3
cut -1");
        println!("deck2 {:?}", deck2.cards);
        assert_eq!(deck2.cards, vec![9, 2, 5, 8, 1, 4, 7, 0, 3, 6]);
    }


    #[test]
    fn test_p1() {
        let deck = Deck::new(10007);
        let deck2 = deck.perform(&input());

        let res = deck2.cards.iter().enumerate()
            .find(|(_, &i)| i == 2019).unwrap();

        let pos = res.0;

        assert_eq!(pos, 7545);
    }
}

fn input() -> String {
    "deal with increment 3
deal into new stack
cut -2846
deal with increment 33
cut -8467
deal into new stack
deal with increment 46
cut 6752
deal with increment 63
deal into new stack
deal with increment 70
deal into new stack
deal with increment 14
cut -1804
deal with increment 68
cut -4936
deal with increment 15
cut -3217
deal with increment 49
cut -1694
deal with increment 58
cut -6918
deal with increment 13
cut -4254
deal with increment 4
deal into new stack
cut 5490
deal into new stack
deal with increment 35
deal into new stack
deal with increment 7
cut 854
deal with increment 46
cut -8619
deal with increment 32
deal into new stack
cut -6319
deal with increment 31
cut 1379
deal with increment 66
cut -7328
deal with increment 55
cut -6326
deal with increment 10
deal into new stack
cut 4590
deal with increment 18
cut -9588
deal with increment 5
cut 3047
deal with increment 24
cut -1485
deal into new stack
deal with increment 53
cut 5993
deal with increment 54
cut -5935
deal with increment 49
cut -3349
deal into new stack
deal with increment 28
cut -4978
deal into new stack
deal with increment 30
cut -1657
deal with increment 50
cut 3732
deal with increment 30
cut 6838
deal with increment 30
deal into new stack
cut -3087
deal with increment 42
deal into new stack
deal with increment 68
cut 3376
deal with increment 51
cut -3124
deal with increment 57
deal into new stack
cut -158
deal into new stack
cut -3350
deal with increment 33
deal into new stack
cut 3387
deal with increment 54
cut 1517
deal with increment 20
cut -3981
deal with increment 64
cut 6264
deal with increment 3
deal into new stack
deal with increment 5
cut 232
deal with increment 29
deal into new stack
cut -5147
deal with increment 51".to_string()
}