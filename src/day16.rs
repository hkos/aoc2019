pub fn part1() {
    println!("Day 16, Part 1");

    let d = Digits::new(&input());
    let res = FFT::phase(&d, 100);

    for i in 0..8 {
        print!("{}", res.d[i]);
    }
    println!();
}

pub fn part2() {
    println!("Day 16, Part 2");

    panic!("brute forcing this won't work");

    let d = Digits::new(&input().repeat(10000));
    let res = FFT::phase(&d, 100);

    for i in 0..8 {
        print!("{}", res.d[i]);
    }
    println!();
}

#[derive(Clone, PartialEq, Debug)]
struct Digits {
    d: Vec<i8>,

}

impl Digits {
    fn new(input: &str) -> Self {
        let mut d = Vec::with_capacity(input.len());

        input.chars().for_each(|c| d.push(c.to_digit(10).unwrap() as i8));

        Digits { d }
    }
}

struct FFT {}

impl FFT {
    fn phase_with_offset(d: &Digits, num: usize) -> String {
        let seven: usize = Self::first_seven(d);
        let out = Self::phase(d, num);

        let res = &out.d[seven..seven + 8];
        res.iter().map(|d| format!("{}", d)).collect()
    }

    fn first_seven(d: &Digits) -> usize {
        0
    }

    fn phase(d: &Digits, num: usize) -> Digits {
        println!("phase {}", num);

        if num == 0 {
            return d.clone();
        } else {
            return FFT::phase(&Self::next_phase(&d), num - 1);
        }
    }

    fn next_phase(d: &Digits) -> Digits {
        let mut next = Vec::with_capacity(d.d.len());

        for row in 0..d.d.len() {
            if row % 1000 == 0 {
                println!("row {} of {}", row, d.d.len());
            }

            let sum: i32 = (0..d.d.len())
                .map(|col| d.d[col] as i32 * Self::get_pattern(row, col) as i32)
                .sum();

            let digit = sum.abs() % 10;
            next.push(digit as i8);
        }


        Digits { d: next }
    }

    // get one digit of the pattern
    fn get_pattern(row: usize, col: usize) -> i8 {
        let pos = col as i32 + 1;
        let pos_scaled = pos / (row + 1) as i32;

        match pos_scaled % 4 {
            0 => 0,
            1 => 1,
            2 => 0,
            3 => -1,
            _ => unreachable!()
        }
    }
}


#[cfg(test)]
mod tests {
    use crate::day16::{Digits, FFT};

    #[test]
    fn test_small1() {
        let d = Digits::new("12345678");

        let one = FFT::next_phase(&d);

        println!("phase 1 {:?}", one);

        assert_eq!(one, Digits { d: vec![4, 8, 2, 2, 6, 1, 5, 8] });

        let four = FFT::phase(&d, 4);

        println!("phase 4  {:?}", four);

        assert_eq!(four, Digits { d: vec![0, 1, 0, 2, 9, 4, 9, 8] });
    }

    #[test]
    fn test_small2() {
        let d = Digits::new("80871224585914546619083218645595");
        let res = FFT::phase(&d, 100);
        assert_eq!(res.d[0..8], [2, 4, 1, 7, 6, 1, 7, 6]);
    }

    #[test]
    fn test_small3() {
        let d = Digits::new("19617804207202209144916044189917");
        let res = FFT::phase(&d, 100);
        assert_eq!(res.d[0..8], [7, 3, 7, 4, 5, 4, 1, 8]);
    }

    #[test]
    fn test_small4() {
        let d = Digits::new("69317163492948606335995924319873");
        let res = FFT::phase(&d, 100);
        assert_eq!(res.d[0..8], [5, 2, 4, 3, 2, 1, 3, 3]);
    }


//    Here is the eight-digit message in the final output list after 100 phases. The message offset given in each input has been highlighted. (Note that the inputs given below are repeated 10000 times to find the actual starting input lists.)
//
//    03036732577212944063491565474664 becomes 84462026.
//    02935109699940807407585447034323 becomes 78725270.
//    03081770884921959731165446850517 becomes 53553731.


    #[test]
    #[ignore]
    fn test_small1_p2() {
        let input = "03036732577212944063491565474664".to_string();
        let input = input.repeat(10000);

        println!("a");

        let d = Digits::new(&input);

        println!("b");

        let res = FFT::phase_with_offset(&d, 100);
        assert_eq!(&res, "84462026");
    }
}

fn input() -> String {
    "59717238168580010599012527510943149347930742822899638247083005855483867484356055489419913512721095561655265107745972739464268846374728393507509840854109803718802780543298141398644955506149914796775885246602123746866223528356493012136152974218720542297275145465188153752865061822191530129420866198952553101979463026278788735726652297857883278524565751999458902550203666358043355816162788135488915722989560163456057551268306318085020948544474108340969874943659788076333934419729831896081431886621996610143785624166789772013707177940150230042563041915624525900826097730790562543352690091653041839771125119162154625459654861922989186784414455453132011498".to_string()
}