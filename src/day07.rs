use crate::intcode::Computer;
use permute::permutations_of;

pub fn part1() {
    println!("Day 07, Part 1");

    let res = find_max(&prog());
    println!("output: {:?}", res);
}

pub fn part2() {
    println!("Day 07, Part 2");

    let res = find_max2(&prog());
    println!("output: {:?}", res);
}


pub fn run_amp(prog: &Vec<i64>, params: &Vec<&i64>) -> i64 {
    let mut input = 0;

    for i in 0..=4 {
        input = Computer::new(prog.clone())
            .set_input(vec![params[i].to_owned(), input])
            .run()[0];
    }

    input
}

pub fn run_feedback(prog: &Vec<i64>, params: &Vec<&i64>) -> i64 {
    let mut computers = Vec::new();

    let p = prog.clone();
    computers.push(Computer::new(p));
    let p = prog.clone();
    computers.push(Computer::new(p));
    let p = prog.clone();
    computers.push(Computer::new(p));
    let p = prog.clone();
    computers.push(Computer::new(p));
    let p = prog.clone();
    computers.push(Computer::new(p));

    let mut out = vec![0];

    for i in 0..=4 {
        let tmp = computers.remove(i)
            .set_input(vec![params[i].to_owned(), *out.last().unwrap()]);
        computers.insert(i, tmp);
        out = computers[i].run();
    }

    // ---out
    let tmp = computers.remove(0).set_input(vec![*out.last().unwrap()]);
    computers.insert(0, tmp);

    let mut i = 0;
    loop {
        let out = computers[i].run();

        if i == 4 && computers[4].is_done() {
            return *out.last().unwrap();
        }

        i = (i + 1) % 5;

        let tmp = computers.remove(i).set_input(vec![*out.last().unwrap()]);
        computers.insert(i, tmp);
    }
}

/// find max for part1
pub fn find_max(prog: &Vec<i64>) -> i64 {
    permutations_of(&[0, 1, 2, 3, 4])
        .map(|i| i.collect::<Vec<_>>())
        .map(|p| run_amp(prog, &p))
        .max()
        .unwrap()
}

/// find max for part2
pub fn find_max2(prog: &Vec<i64>) -> i64 {
    permutations_of(&[5, 6, 7, 8, 9])
        .map(|i| i.collect::<Vec<_>>())
        .map(|p| run_feedback(prog, &p))
        .max()
        .unwrap()
}

#[cfg(test)]
mod tests {
    use crate::day07::{run_amp, find_max, run_feedback, find_max2, prog};

    #[test]
    fn test_small1() {
        let mem = vec![3, 15, 3, 16, 1002, 16, 10, 16, 1, 16, 15, 15, 4, 15, 99, 0, 0];
        let res = run_amp(&mem, &vec![&4, &3, &2, &1, &0]);
        assert_eq!(res, 43210);

        let res = find_max(&mem);
        assert_eq!(res, 43210);
    }

    #[test]
    fn test_small2() {
        let mem = vec![3, 23, 3, 24, 1002, 24, 10, 24, 1002, 23, -1, 23, 101, 5, 23, 23, 1, 24, 23, 23, 4, 23, 99, 0, 0];
        let res = find_max(&mem);
        assert_eq!(res, 54321);
    }


    #[test]
    fn test_small3() {
        let mem = vec![3, 31, 3, 32, 1002, 32, 10, 32, 1001, 31, -2, 31, 1007, 31, 0, 33,
                       1002, 33, 7, 33, 1, 33, 31, 31, 1, 32, 31, 31, 4, 31, 99, 0, 0, 0];
        let out = run_amp(&mem, &vec![&1, &0, &4, &3, &2]);
        assert_eq!(out, 65210);
    }

    #[test]
    fn test_pt2_small4() {
        let mem = vec![3, 26, 1001, 26, -4, 26, 3, 27, 1002, 27, 2, 27, 1, 27, 26,
                       27, 4, 27, 1001, 28, -1, 28, 1005, 28, 6, 99, 0, 0, 5];
        let out = run_feedback(&mem, &vec![&9, &8, &7, &6, &5]);
        assert_eq!(out, 139629729);

        let out = find_max2(&mem);
        assert_eq!(out, 139629729);
    }

    #[test]
    fn test_pt2_small5() {
        let mem = vec![3, 52, 1001, 52, -5, 52, 3, 53, 1, 52, 56, 54, 1007, 54, 5, 55, 1005, 55, 26, 1001, 54,
                       -5, 54, 1105, 1, 12, 1, 53, 54, 53, 1008, 54, 0, 55, 1001, 55, 1, 55, 2, 53, 55, 53, 4,
                       53, 1001, 56, -1, 56, 1005, 56, 6, 99, 0, 0, 0, 0, 10];

        let out = find_max2(&mem);
        assert_eq!(out, 18216);
    }

    #[test]
    fn test_pt1() {
        let res = find_max(&prog());
        assert_eq!(res, 398674);
    }

    #[test]
    fn test_pt2() {
        let res = find_max2(&prog());
        assert_eq!(res, 39431233);
    }
}

fn prog() -> Vec<i64> {
    vec![3, 8, 1001, 8, 10, 8, 105, 1, 0, 0, 21, 38, 63, 76, 93, 118, 199, 280, 361, 442, 99999, 3, 9, 101, 3, 9, 9, 102, 3, 9, 9, 101, 4, 9, 9, 4, 9, 99, 3, 9, 1002, 9, 2, 9, 101, 5, 9, 9, 1002, 9, 5, 9, 101, 5, 9, 9, 1002, 9, 4, 9, 4, 9, 99, 3, 9, 101, 2, 9, 9, 102, 3, 9, 9, 4, 9, 99, 3, 9, 101, 2, 9, 9, 102, 5, 9, 9, 1001, 9, 5, 9, 4, 9, 99, 3, 9, 102, 4, 9, 9, 1001, 9, 3, 9, 1002, 9, 5, 9, 101, 2, 9, 9, 1002, 9, 2, 9, 4, 9, 99, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 1001, 9, 1, 9, 4, 9, 3, 9, 1001, 9, 1, 9, 4, 9, 3, 9, 1001, 9, 1, 9, 4, 9, 3, 9, 1001, 9, 2, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 101, 2, 9, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 1001, 9, 1, 9, 4, 9, 3, 9, 101, 2, 9, 9, 4, 9, 99, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 1001, 9, 2, 9, 4, 9, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9, 101, 1, 9, 9, 4, 9, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9, 1001, 9, 1, 9, 4, 9, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9, 1001, 9, 1, 9, 4, 9, 99, 3, 9, 101, 1, 9, 9, 4, 9, 3, 9, 101, 2, 9, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 101, 2, 9, 9, 4, 9, 3, 9, 1001, 9, 2, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9, 1001, 9, 1, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 99, 3, 9, 1001, 9, 1, 9, 4, 9, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 1001, 9, 2, 9, 4, 9, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9, 101, 2, 9, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 101, 1, 9, 9, 4, 9, 3, 9, 1001, 9, 2, 9, 4, 9, 99, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9, 101, 2, 9, 9, 4, 9, 3, 9, 101, 1, 9, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 1001, 9, 2, 9, 4, 9, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9, 101, 1, 9, 9, 4, 9, 3, 9, 101, 2, 9, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 99]
}