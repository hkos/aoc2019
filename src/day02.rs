use crate::intcode::Computer;

pub fn part1() {
    println!("Day 02, Part 1");

    let mut mem = program();
    mem[1] = 12;
    mem[2] = 2;

    let mut comp = Computer::new(mem);
    comp.run();
    println!("memory 0: {}\n", comp.get_mem(0));
}

pub fn part2() {
    println!("Day 02, Part 2");

    println!("100 * noun + verb: {}\n",
             find_noun_and_verb(&program(), 19690720));
}

fn find_noun_and_verb(mem: &[i64], target_output: i64) -> i64 {
    for noun in 0..=99 {
        for verb in 0..=99 {
            let mut test = mem.clone().to_vec();

            test[1] = noun;
            test[2] = verb;

            let mut comp = Computer::new(test);
            comp.run();

            let res = comp.get_mem(0);
            if res == target_output {
                return 100 * noun + verb;
            }
        }
    }
    panic!("no result found");
}

fn program() -> Vec<i64> {
    vec![1, 0, 0, 3, 1, 1, 2, 3, 1, 3, 4, 3, 1, 5, 0, 3, 2, 13, 1, 19, 1,
         9, 19, 23, 2, 23, 13, 27, 1, 27, 9, 31, 2, 31, 6, 35, 1, 5, 35,
         39, 1, 10, 39, 43, 2, 43, 6, 47, 1, 10, 47, 51, 2, 6, 51, 55, 1,
         5, 55, 59, 1, 59, 9, 63, 1, 13, 63, 67, 2, 6, 67, 71, 1, 5, 71,
         75, 2, 6, 75, 79, 2, 79, 6, 83, 1, 13, 83, 87, 1, 9, 87, 91, 1,
         9, 91, 95, 1, 5, 95, 99, 1, 5, 99, 103, 2, 13, 103, 107, 1, 6,
         107, 111, 1, 9, 111, 115, 2, 6, 115, 119, 1, 13, 119, 123, 1,
         123, 6, 127, 1, 127, 5, 131, 2, 10, 131, 135, 2, 135, 10, 139, 1,
         13, 139, 143, 1, 10, 143, 147, 1, 2, 147, 151, 1, 6, 151, 0, 99,
         2, 14, 0, 0]
}


#[cfg(test)]
mod tests {
    use crate::day02;
    use crate::intcode::Computer;

    #[test]
    fn test_examples() {
        let mem = vec![1, 9, 10, 3, 2, 3, 11, 0, 99, 30, 40, 50];
        let mut comp = Computer::new(mem);
        comp.run();
        assert_eq!(comp.get_mem(0), 3500);

        let mem = vec![1, 0, 0, 0, 99];
        let mut comp = Computer::new(mem);
        comp.run();
        assert_eq!(comp.get_mem(0), 2);

        let mem = vec![2, 3, 0, 3, 99];
        let mut comp = Computer::new(mem);
        comp.run();
        assert_eq!(comp.get_mem(3), 6);

        let mem = vec![2, 4, 4, 5, 99, 0];
        let mut comp = Computer::new(mem);
        comp.run();
        assert_eq!(comp.get_mem(5), 9801);

        let mem = vec![1, 1, 1, 4, 99, 5, 6, 0, 99];
        let mut comp = Computer::new(mem);
        comp.run();
        assert_eq!(comp.get_mem(0), 30);
    }

    #[test]
    fn test_aoc_part1() {
        let mut prog = day02::program();
        prog[1] = 12;
        prog[2] = 2;

        let mut comp = Computer::new(prog);
        comp.run();
        assert_eq!(comp.get_mem(0), 3306701);
    }

    #[test]
    fn test_aoc_part2() {
        let res = day02::find_noun_and_verb(&day02::program(), 19690720);
        assert_eq!(res, 7621);
    }
}
