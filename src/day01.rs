pub fn part1() {
    println!("Day 01, Part 1");

    let sum: i32 = modules().iter().map(|&mass| fuel(mass)).sum();
    println!("sum fuel for modules: {}\n", sum);
}

pub fn part2() {
    println!("Day 01, Part 2");

    let sum: i32 = modules().iter().map(|&mass| fuel_plus(mass)).sum();
    println!("sum with fuel for fuel: {}\n", sum);
}

fn fuel(mass: i32) -> i32 {
    (mass / 3) - 2
}

fn fuel_plus(mass: i32) -> i32 {
    let mut sum = fuel(mass);

    let mut x = sum;
    while fuel(x) > 0 {
        sum = sum + fuel(x);
        x = fuel(x);
    }

    sum
}

fn modules() -> Vec<i32> {
    vec![119031, 111204, 75773, 95544, 69987, 147194, 74024, 100438, 86116, 89945, 144856, 123315, 64102, 55491, 95959, 149174, 66810, 134674, 88921, 124270, 60833, 125667, 84885, 57688, 89059, 126854, 93633, 103791, 104295, 137762, 101216, 138060, 103271, 95822, 102000, 66821, 126916, 104629, 87710, 79852, 87852, 149281, 92055, 50969, 62626, 112069, 68560, 66131, 139961, 89456, 100536, 51338, 51075, 112858, 134878, 137702, 60091, 111576, 70517, 131524, 56162, 148346, 62696, 110191, 141106, 54858, 66248, 86402, 132012, 96367, 95319, 133879, 115031, 77875, 129470, 146650, 70048, 147454, 123076, 74563, 94228, 59920, 147986, 92398, 51890, 92686, 110452, 85205, 67482, 87931, 69535, 73948, 114576, 65958, 53081, 132809, 76088, 74553, 121820, 121214, ]
}


#[cfg(test)]
mod tests {
    use crate::day01::{fuel, fuel_plus};

    #[test]
    fn test_fuel() {
        assert_eq!(fuel(12), 2);
        assert_eq!(fuel(14), 2);
        assert_eq!(fuel(100756), 33583);
    }

    #[test]
    fn test_fuel_plus() {
        assert_eq!(fuel_plus(100756), 50346);
    }
}
