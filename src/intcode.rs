/// intcode Computer
/// for advent of code 2019
#[derive(Clone)]
enum Mode { Position, Immediate, Relative }

impl Mode {
    fn new(m: i64) -> Self {
        match m {
            0 => Mode::Position,
            1 => Mode::Immediate,
            2 => Mode::Relative,
            _ => panic!("illegal mode")
        }
    }
}

#[derive(Clone)]
enum Opcode {
    Add,
    Mul,
    Input,
    Output,
    JumpIfTrue,
    JumpIfFalse,
    LessThan,
    Equals,
    AdjustBase,
    Halt,
}

impl Opcode {
    fn new(m: i64) -> Self {
        match m {
            1 => Opcode::Add,
            2 => Opcode::Mul,
            3 => Opcode::Input,
            4 => Opcode::Output,
            5 => Opcode::JumpIfTrue,
            6 => Opcode::JumpIfFalse,
            7 => Opcode::LessThan,
            8 => Opcode::Equals,
            9 => Opcode::AdjustBase,
            99 => Opcode::Halt,
            _ => panic!("illegal opcode {}", m)
        }
    }
}

#[derive(Clone)]
struct Instruction {
    opcode: Opcode,
    modes: Vec<Mode>,
}

impl Instruction {
    fn get_mode(&self, param: usize) -> &Mode {
        &self.modes[param - 1]
    }
}

#[derive(Clone)]
pub struct Computer {
    ip: usize,
    relative_base: i64,
    cur_instruction: Option<Instruction>,
    mem: Vec<i64>,
    input: Vec<i64>,
    output: Vec<i64>,
    done: bool,
}

impl Computer {
    pub fn new(mem: Vec<i64>) -> Self {
        Self {
            ip: 0,
            relative_base: 0,
            cur_instruction: None,
            mem,
            input: Vec::new(),
            output: Vec::new(),
            done: false,
        }
    }

    pub fn get_mem(&self, addr: usize) -> i64 {
        self.mem[addr]
    }

    pub fn set_mem(&mut self, addr: usize, val: i64) { self.mem[addr] = val; }


    pub fn set_input(mut self, input: Vec<i64>) -> Self {
        self.input = input;
        self
    }

    pub fn take_output(&mut self) -> Vec<i64> {
        let out = self.output.clone();
        self.output = Vec::new();
        out
    }

    pub fn update_input(&mut self, input: Vec<i64>) {
        self.input = input;
    }

    pub fn append_input(&mut self, input: &mut Vec<i64>) {
        self.input.append(input);
    }

    pub fn has_input(&self) -> bool {
        !self.input.is_empty()
    }


    /// has the Computer run to completion - or has it stopped because
    /// of missing input?
    pub fn is_done(&self) -> bool {
        self.done
    }

    /// this will either start the program from the beginning,
    /// or resume execution (if input has run out previously).
    pub fn run(&mut self) -> Vec<i64> {
        let input = self.input.clone();
        self.input.clear();

        let mut input = input.iter();

        loop {
            self.parse_instr();
            match self.cur_instruction.as_ref().unwrap().opcode {
                Opcode::Add => self.calc(|a, b| a + b),
                Opcode::Mul => self.calc(|a, b| a * b),
                Opcode::Input => match input.next() {
                    None => return self.output.clone(), // not enough input - stopping for now
                    Some(&x) => self.take_input(x)
                }
                Opcode::Output => self.store_output(),
                Opcode::JumpIfTrue => self.jump_if(|i| i != 0),
                Opcode::JumpIfFalse => self.jump_if(|i| i == 0),
                Opcode::LessThan => self.compare(|a, b| a < b),
                Opcode::Equals => self.compare(|a, b| a == b),
                Opcode::AdjustBase => self.set_base(),
                Opcode::Halt => {
                    self.done = true;
                    return self.output.clone();
                }
            }
        }
    }


    // --- utils ---

    fn bool_to_int(b: bool) -> i64 {
        if b { 1 } else { 0 }
    }

    /// parse the current instruction and store in "self.cur_instruction"
    fn parse_instr(&mut self) {
        let full_opcode = self.mem[self.ip];

        let opcode = Opcode::new(full_opcode % 100);

        let m = full_opcode / 100;

        let mut modes = Vec::new();
        modes.push(Mode::new(m % 10));
        modes.push(Mode::new((m / 10) % 10));
        modes.push(Mode::new((m / 100) % 10));

        self.cur_instruction = Some(Instruction { opcode, modes });
    }

    /// get n-th param (param is one-based)
    fn get_param(&mut self, param: usize) -> i64 {
        let addr = self.get_addr(param);
        self.resize_mem(addr);
        self.mem[addr]
    }

    /// store value according to n-th param (param is one-based)
    fn store_param(&mut self, param: usize, val: i64) {
        let addr = self.get_addr(param);
        self.resize_mem(addr);
        self.mem[addr] = val;
    }

    /// get address of n-th param (param is one-based)
    fn get_addr(&self, param: usize) -> usize {
        match self.cur_instruction.as_ref()
            .expect("no current instruction loaded")
            .get_mode(param) {
            Mode::Position => self.mem[self.ip + param] as usize,
            Mode::Immediate => self.ip + param,
            Mode::Relative => (self.mem[self.ip + param] + self.relative_base) as usize,
        }
    }

    /// resize memory so that "addr" is a valid address
    fn resize_mem(&mut self, addr: usize) {
        if addr + 1 > self.mem.len() {
            self.mem.resize(addr + 1, 0);
        }
    }

    // --- opcodes ---

    fn calc<F>(&mut self, func: F) where F: Fn(i64, i64) -> i64 {
        let val = func(self.get_param(1), self.get_param(2));
        self.store_param(3, val);
        self.ip += 4;
    }

    fn take_input(&mut self, input: i64) {
        self.store_param(1, input);
        self.ip += 2;
    }

    fn store_output(&mut self) {
        let val = self.get_param(1);
        self.output.push(val);
        self.ip += 2;
    }

    fn jump_if<F>(&mut self, test: F) where F: Fn(i64) -> bool {
        if test(self.get_param(1)) {
            self.ip = self.get_param(2) as usize
        } else {
            self.ip += 3;
        }
    }

    fn compare<F>(&mut self, cmp: F) where F: Fn(i64, i64) -> bool {
        let compare = cmp(self.get_param(1), self.get_param(2));
        self.store_param(3, Self::bool_to_int(compare));

        self.ip += 4;
    }

    fn set_base(&mut self) {
        self.relative_base += self.get_param(1);
        self.ip += 2;
    }
}