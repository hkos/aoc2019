use chrono::prelude::*;

pub fn part1() {
    println!("Day 12, Part 1");

    let t1 = Local::now().timestamp_nanos();

    let mut system = System::load(&system());
    system.run(1000);
    println!("energy: {}", system.get_energy());

    let t2 = Local::now().timestamp_nanos();
    println!("[runtime {}us]\n", (t2 - t1) / 1000);
}

pub fn part2() {
    println!("Day 12, Part 2");

    let t1 = Local::now().timestamp_millis();
    let system = System::load(&system());

    println!("cycle: {}", system.cycle_len());

    let t2 = Local::now().timestamp_millis();
    println!("[runtime {}ms]\n", t2 - t1);
}


#[derive(PartialEq, Eq, Clone, Debug, Hash)]
struct Moon {
    pos: [i32; 3],
    velocity: [i32; 3],
}

impl Moon {
    fn new(pos: [i32; 3]) -> Self {
        Moon { pos, velocity: [0, 0, 0] }
    }

    fn get_energy(&self) -> usize {
        let pot: usize = self.pos.iter().map(|x| x.abs() as usize).sum();
        let kin: usize = self.velocity.iter().map(|x| x.abs() as usize).sum();
        pot * kin
    }
}

#[derive(Clone)]
struct System {
    moons: Vec<Moon>
}

impl System {
    fn get_num(s: &str) -> i32 {
        // FIXME: this is not nice
        let s = s.replace(">", "");
        let x: Vec<_> = s.split('=').collect();
        x[1].parse::<i32>().expect("couldn't parse number")
    }

    pub fn load(s: &str) -> System {
        let mut moons = Vec::new();
        for l in s.lines() {
            let s: Vec<_> = l.split(',').collect();

            moons.push(Moon::new([Self::get_num(s[0]),
                Self::get_num(s[1].clone()),
                Self::get_num(s[2].clone())]));
        }
        System { moons }
    }

    fn apply_gravity(&mut self, dim: usize) {
        for m in 0..self.moons.len() {
            for o in 0..self.moons.len() {
                if m != o {
                    // apply force from each o to m
                    if self.moons[o].pos[dim] > self.moons[m].pos[dim] {
                        self.moons[m].velocity[dim] += 1;
                    } else if self.moons[o].pos[dim] < self.moons[m].pos[dim] {
                        self.moons[m].velocity[dim] -= 1;
                    }
                }
            }
        }
    }

    fn apply_velocity(&mut self, dim: usize) {
        for m in self.moons.iter_mut() {
            m.pos[dim] += m.velocity[dim];
        }
    }

    fn get_energy(&self) -> usize {
        self.moons.iter().map(|m| m.get_energy()).sum()
    }

    pub fn print(&self) {
        for m in self.moons.iter() {
            println!("pos=<x={}, y={}, z={}>, vel=<x={}, y={}, z={}>",
                     m.pos[0], m.pos[1], m.pos[2],
                     m.velocity[0], m.velocity[1], m.velocity[2]);
        }
    }

    /// run one step in dimension "dim"
    fn step_in_dim(&mut self, dim: usize) {
        self.apply_gravity(dim);
        self.apply_velocity(dim);
    }

    /// run "steps" steps in all dimensions
    fn run(&mut self, steps: usize) {
        for _ in 0..steps {
            for dim in 0..3 {
                self.step_in_dim(dim);
            }
        }
    }

    /// find cycle length for dimension "dim"
    fn find_cycle_in_dim(&mut self, dim: usize) -> usize {

        // FIXME:
        // this approach assumes cycles will return to the initial state
        // (as opposed to some n_th state).
        // apparently this assumption is ok for the values in this puzzle
        // -> but is it necessarily true?

        let first = self.moons.clone();

        let mut n = 0;
        loop {
            self.step_in_dim(dim);
            n += 1;

            if first == self.moons {
                return n;
            }
        }
    }

    pub fn cycle_len(&self) -> usize {
        use gcd::Gcd;

        // run each dimension separately (clone initial state per dimension)
        let cycles: Vec<_> = (0..3).map(|d|
            self.clone().find_cycle_in_dim(d)).collect();

        let tmp = cycles[0] * cycles[1] / cycles[0].gcd(cycles[1]);
        tmp * cycles[2] / tmp.gcd(cycles[2])
    }
}


#[cfg(test)]
mod tests {
    use crate::day12::{System, system};

    #[test]
    fn test_small1() {
        let test = "<x=-1, y=0, z=2>
<x=2, y=-10, z=-7>
<x=4, y=-8, z=8>
<x=3, y=5, z=-1>";
        let mut system = System::load(test);

        system.run(10);
        system.print();

        system.print();
        let energy = system.get_energy();
        println!("energy: {}", energy);

        assert_eq!(energy, 179);
    }


    #[test]
    fn test_small2() {
        let test = "<x=-8, y=-10, z=0>
<x=5, y=5, z=10>
<x=2, y=-7, z=3>
<x=9, y=-8, z=-3>";
        let mut system = System::load(test);

        system.run(100);

        let energy = system.get_energy();
        println!("energy: {}", energy);

        assert_eq!(energy, 1940);
    }


    #[test]
    fn test_p1() {
        let mut system = System::load(&system());
        system.run(1000);

        let energy = system.get_energy();
        println!("energy: {}", energy);

        assert_eq!(energy, 9441);
    }

    #[test]
    fn test_p2_small1() {
        let test = "<x=-1, y=0, z=2>
<x=2, y=-10, z=-7>
<x=4, y=-8, z=8>
<x=3, y=5, z=-1>";
        let system = System::load(test);

        let cycle = system.cycle_len();
        assert_eq!(cycle, 2772);
    }

    #[test]
    fn test_p2() {
        let system = System::load(&system());
        let cycle = system.cycle_len();

        assert_eq!(cycle, 503560201099704);
    }
}

fn system() -> String {
    "<x=17, y=-7, z=-11>
<x=1, y=4, z=-1>
<x=6, y=-2, z=-6>
<x=19, y=11, z=9>".to_string()
}