use crate::intcode::Computer;
use crate::day11::Dir::{U, L, D, R};
use std::collections::HashSet;

pub fn part1() {
    println!("Day 11, Part 1");

    let comp = Computer::new(prog());
    let mut robot = Robot::new(comp);

    let painted = robot.run(false);

    robot.print_state();

    println!("painted: {:?}\n", painted);
}

pub fn part2() {
    println!("Day 11, Part 2");

    let comp = Computer::new(prog());
    let mut robot = Robot::new(comp);

    robot.run(true);

    robot.print_state();
}


#[derive(PartialEq, Eq, Hash, Clone, Debug)]
struct Coord(i32, i32);

#[derive(Debug)]
enum Dir { L, R, U, D }

struct Robot {
    white: HashSet<Coord>,
    painted: HashSet<Coord>,
    direction: Dir,
    pos: Coord,
    comp: Computer,
}

impl Robot {
    pub fn new(comp: Computer) -> Self {
        Robot {
            white: HashSet::new(),
            painted: HashSet::new(),
            direction: U,
            pos:
            Coord(0, 0),
            comp,
        }
    }

    pub fn run(&mut self, init_white: bool) -> usize {
        if init_white {
            self.white.insert(self.pos.clone());
        }

        loop {
            let input = if self.white.contains(&self.pos) { 1 } else { 0 };
            self.comp.update_input(vec![input]);

            // run the next step of the robot
            self.comp.run();

            if self.comp.is_done() {
                return self.painted.len();
            }

            let out = self.comp.take_output();
            self.painted.insert(self.pos.clone());

            if out[0] == 1 {
                self.white.insert(self.pos.clone());
            } else {
                self.white.remove(&self.pos);
            }

            // rotate
            match out[1] {
                0 => self.turn_left(),
                1 => self.turn_right(),
                _ => panic!("unexpected output param for rotation")
            }

            // move one step ahead
            self.step();
        }
    }

    pub fn print_state(&self) {
        let top = self.painted.iter().map(|c| c.1).min().unwrap();
        let bottom = self.painted.iter().map(|c| c.1).max().unwrap();
        let left = self.painted.iter().map(|c| c.0).min().unwrap();
        let right = self.painted.iter().map(|c| c.0).max().unwrap();

        for y in top..=bottom {
            for x in left..=right {
                if self.pos == Coord(x, y) {
                    match self.direction {
                        L => print!("<"),
                        R => print!(">"),
                        U => print!("^"),
                        D => print!("v"),
                    }
                } else {
                    match (self.painted.contains(&Coord(x, y)),
                           self.white.contains(&Coord(x, y))) {
                        (false, _) => print!("_"),
                        (true, false) => print!("'"),
                        (true, true) => print!("#"),
                    }
                }
            }
            println!();
        }
        println!("state after move: pos {:?}, rotation {:?}\n",
                 self.pos, self.direction);
        println!();
    }

    fn turn_right(&mut self) {
        match self.direction {
            L => self.direction = U,
            U => self.direction = R,
            R => self.direction = D,
            D => self.direction = L,
        }
    }

    fn turn_left(&mut self) {
        match self.direction {
            L => self.direction = D,
            U => self.direction = L,
            R => self.direction = U,
            D => self.direction = R,
        }
    }

    fn step(&mut self) {
        match self.direction {
            L => self.pos.0 -= 1,
            R => self.pos.0 += 1,
            U => self.pos.1 -= 1,
            D => self.pos.1 += 1,
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::intcode::Computer;
    use crate::day11::{prog, Robot};

    #[test]
    fn test_p1() {
        let prog = prog();
        let comp = Computer::new(prog);
        let mut robot = Robot::new(comp);

        let painted = robot.run(false);
        assert_eq!(painted, 2511);
    }

    #[test]
    fn test_p2() {
        let prog = prog();
        let comp = Computer::new(prog);
        let mut robot = Robot::new(comp);

        let painted = robot.run(true);

        assert_eq!(painted, 248);
        assert_eq!(robot.white.len(), 95);
    }
}

fn prog() -> Vec<i64> {
    vec![3, 8, 1005, 8, 328, 1106, 0, 11, 0, 0, 0, 104, 1, 104, 0, 3, 8, 1002, 8, -1, 10, 1001, 10, 1, 10, 4, 10, 1008, 8, 0, 10, 4, 10, 1001, 8, 0, 29, 1, 104, 7, 10, 3, 8, 1002, 8, -1, 10, 101, 1, 10, 10, 4, 10, 1008, 8, 0, 10, 4, 10, 1001, 8, 0, 55, 1, 2, 7, 10, 1006, 0, 23, 3, 8, 102, -1, 8, 10, 1001, 10, 1, 10, 4, 10, 1008, 8, 0, 10, 4, 10, 1001, 8, 0, 84, 1006, 0, 40, 1, 1103, 14, 10, 1, 1006, 16, 10, 3, 8, 102, -1, 8, 10, 101, 1, 10, 10, 4, 10, 108, 1, 8, 10, 4, 10, 1002, 8, 1, 116, 1006, 0, 53, 1, 1104, 16, 10, 3, 8, 102, -1, 8, 10, 101, 1, 10, 10, 4, 10, 1008, 8, 1, 10, 4, 10, 102, 1, 8, 146, 2, 1104, 9, 10, 3, 8, 102, -1, 8, 10, 101, 1, 10, 10, 4, 10, 1008, 8, 1, 10, 4, 10, 1001, 8, 0, 172, 1006, 0, 65, 1, 1005, 8, 10, 1, 1002, 16, 10, 3, 8, 102, -1, 8, 10, 1001, 10, 1, 10, 4, 10, 108, 0, 8, 10, 4, 10, 102, 1, 8, 204, 2, 1104, 9, 10, 1006, 0, 30, 3, 8, 102, -1, 8, 10, 101, 1, 10, 10, 4, 10, 108, 0, 8, 10, 4, 10, 102, 1, 8, 233, 2, 1109, 6, 10, 1006, 0, 17, 1, 2, 6, 10, 3, 8, 102, -1, 8, 10, 101, 1, 10, 10, 4, 10, 108, 1, 8, 10, 4, 10, 102, 1, 8, 266, 1, 106, 7, 10, 2, 109, 2, 10, 2, 9, 8, 10, 3, 8, 102, -1, 8, 10, 101, 1, 10, 10, 4, 10, 1008, 8, 1, 10, 4, 10, 1001, 8, 0, 301, 1, 109, 9, 10, 1006, 0, 14, 101, 1, 9, 9, 1007, 9, 1083, 10, 1005, 10, 15, 99, 109, 650, 104, 0, 104, 1, 21102, 1, 837548789788, 1, 21101, 0, 345, 0, 1106, 0, 449, 21101, 0, 846801511180, 1, 21101, 0, 356, 0, 1106, 0, 449, 3, 10, 104, 0, 104, 1, 3, 10, 104, 0, 104, 0, 3, 10, 104, 0, 104, 1, 3, 10, 104, 0, 104, 1, 3, 10, 104, 0, 104, 0, 3, 10, 104, 0, 104, 1, 21101, 235244981271, 0, 1, 21101, 403, 0, 0, 1105, 1, 449, 21102, 1, 206182744295, 1, 21101, 0, 414, 0, 1105, 1, 449, 3, 10, 104, 0, 104, 0, 3, 10, 104, 0, 104, 0, 21102, 837896937832, 1, 1, 21101, 0, 437, 0, 1106, 0, 449, 21101, 867965862668, 0, 1, 21102, 448, 1, 0, 1106, 0, 449, 99, 109, 2, 22102, 1, -1, 1, 21101, 40, 0, 2, 21102, 1, 480, 3, 21101, 0, 470, 0, 1106, 0, 513, 109, -2, 2106, 0, 0, 0, 1, 0, 0, 1, 109, 2, 3, 10, 204, -1, 1001, 475, 476, 491, 4, 0, 1001, 475, 1, 475, 108, 4, 475, 10, 1006, 10, 507, 1101, 0, 0, 475, 109, -2, 2106, 0, 0, 0, 109, 4, 1201, -1, 0, 512, 1207, -3, 0, 10, 1006, 10, 530, 21102, 1, 0, -3, 22102, 1, -3, 1, 21201, -2, 0, 2, 21102, 1, 1, 3, 21102, 549, 1, 0, 1106, 0, 554, 109, -4, 2105, 1, 0, 109, 5, 1207, -3, 1, 10, 1006, 10, 577, 2207, -4, -2, 10, 1006, 10, 577, 21202, -4, 1, -4, 1106, 0, 645, 21202, -4, 1, 1, 21201, -3, -1, 2, 21202, -2, 2, 3, 21101, 596, 0, 0, 1106, 0, 554, 21201, 1, 0, -4, 21102, 1, 1, -1, 2207, -4, -2, 10, 1006, 10, 615, 21101, 0, 0, -1, 22202, -2, -1, -2, 2107, 0, -3, 10, 1006, 10, 637, 22102, 1, -1, 1, 21101, 637, 0, 0, 105, 1, 512, 21202, -2, -1, -2, 22201, -4, -2, -4, 109, -5, 2106, 0, 0]
}